## Description

In-car monitoring/logging device with graphical output:

* throttle (0% to 100%)
* engine RPM (usually 750 to 4000)
* engine load (0% to 100%)
* speed (both wheels and GPS, in km/hour)
* short- and medium-term fuel economy predictions (as reported by OBD-II)
* inertial data (acceleration and gyro, 100 Hz sampling)

![live example](./esempio.jpg)

## Hardware

* Beagleboard XM running Arch Linux
* u-Blox USB GPS unit
* Sparkfun 9DOF IMU (inertial measurement unit)
* ELM327-compatible USB OBD-II (on-board diagnostics)
* Sunfounder 7" TFT HDMI display
* also: 12V to 5V adapter, USB wifi stick

## Services

The only service required is the stock *gpsd* daemon to get TPV (time, position, velocity) events via socket queries/listening.

IMU is a serial port device, read-only (no need to query).

OBD-II interface appears as a serial port device (querying mostly with "AT"-like commands).

## Software

Sensor threads send events to the main loop *mpsc* channel. Main thread does filtering, graphical screen updating, and shipping packets to a save'n'sync thread.

OBD-II error codes are shown (if reported) for a few seconds at start-up before entering the main loop.

Threads running:

* *gpsd.rs:* GPS listener (also updates date/time);
* *imu.rs:* inertial sensor data logger;
* *obd.rs:* OBD-II querying;
* *buffer.rs:* buffered data save and sync.

Graphical libraries (virtual console *vcsa.rs* and framebuffer *fb.rs)* write directly to display framebuffers.

Three mandatory command-line arguments: OBD dongle serial port, IMU dongle serial port, data directory (where to save binary log files).

## Cross-compiling

Pre-requisite for a Beagleboard: an ARM/gcc toolchain *(sudo apt install gcc-arm-linux-gnueabihf* on Ubuntu) and ARM/gnueabihf Rust target *(rustup target install armv7-unknown-linux-gnueabihf)*; you may have to edit the *./cargo/config* accordingly.

Compiling:

    cargo build --target=armv7-unknown-linux-gnueabihf --release

## Unsafe's

*buffer.rs:*

- needed an unsafe *slice::from_raw_parts* when serializing Info enums to disk.

*fb.rs:*

- the *mmap* thing is done via *libc* functions (all unsafe by definition);
- the *leftscroll* was faster using a *ptr::copy* with overlapping pointers instead of a loop (duh, never benchmarked them again since Rust 1.16 release);
- same applies to *rightscroll*

*main.rs:*

- importing C library functions *sync()* and *clock()*

Note: to debug (simulate) IMU and OBD modules, use an empty string as the respective command-line parameter.

## QuickFAQ

*New releases?* This is a personal project tailored to my likes and my old Fiat Punto car (which exposes an OBD-II interface but not a working CAN-bus); it's been running basically without modifications since early 2016 (this is why I don't use the most recent Rust language features). I do not plan to add new features until I'll switch to a new car.

*Binary file format?* Always 24 bytes packets. Check out the *Info* structure in the main source. See also the *datlist* script to disassemble *.dat* files.

*What graphical library do you use?* None. Linux kernel initializes the screen framebuffer, I just had to *mmap* the usual */dev/fb0* framebuffer device (exposing pixels as an array of *u32* 0RGB values, in my case 1024 per row, 600 contiguous rows) and open */dev/vcsa1* virtual console "with attributes" to write text without doing any font rendering.

*How do you transfer/visualize data files?* Using *rsync* via wifi (autoconnect in the parking spot) and an unreleased Ruby script to build *gnuplot* graphs.

*OBD-II is slow.* Yes, especially if the car only supports ISO 14230-4 or SAE J1850 VPW, both running at about 10 kilobits/second: every single parameter query, except battery voltage (which is a feature of the interface), requires about 200-250 milliseconds; this is why I query a very few parameters (and some of them more often than others). No, the CAN thing doesn't work on my old car.

*Real-life advantage?* Something to stare at when stuck in the traffic. And the joy of not seeing anymore an OBD warning about a secondary fan. And the full GPS log on my home PC. Even if my car does not emit more useful OBD-II values (say, brake pedal usage), I guess I probably have more info on screen than the usual electric car owner.

*Internet?* It never connects to the internet. The wifi access point at the parking spot lays behind a firewall and a restricted *rrsync* transfer, without default routing. Wifi+rsync is only meant to move log files from the car to the home server.

![another example](./example.jpg)
